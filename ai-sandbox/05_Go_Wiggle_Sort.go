// Link to test answer on LeetCode: https://leetcode.com/problems/wiggle-sort-ii/?envType=featured-list

// Wiggle Sort Problem: We are given an integer array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]... Provide two different solutions for us to choose from.