// Link to test answer on LeetCode: https://leetcode.com/problems/count-primes/?envType=featured-list

// Count Primes Problem : We are given an integer n, return the number of prime numbers that are less than n. Provide two different solutions for us to choose from.