# Welcome to the GitLab Duo Workshop :fox:

[[_TOC_]]

## :dart: Introduction

The goal of this workshop is to give you a look into all of the features the GitLab AI team is developing, not just Code Suggestions. We put a big emphasis on trying to help everyone throughout the entire Software Delivery Life Cycle (SDLC) and not just developers on their coding tasks.

**PLEASE NOTE:** Some of these features are in the [experimental phase](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment), which means they may be prone to outages as the dev team is actively working on enhancing them. Check out our docs [here](https://docs.gitlab.com/ee/user/ai_features.html) to see each feature's Maturity level.

## :checkered_flag: Get Started

### 1\. Open GitLab Duo Chat

Throughout this workshop, you will get AI-generated support from GitLab Duo Chat.

- [ ] To open the chat:
  1. In the lower-left corner, select the `Help` icon.
  2. Select `GitLab Duo Chat`. A drawer opens on the right side of your screen.
  3. Click on one of the predefined questions (or ask your own question)
  4. Then clear the context by typing `/reset`
  5. Finally, delete all previous conversations with the `/clean` message.

### 2\. Enable AI/ML features

 - All of the AI features have already been [enabled](https://docs.gitlab.com/ee/user/ai_features.html#enable-aiml-features) in this environment.
    - AI features listed as Experimental and Beta must be turned at the group level by a user with the Owner role.
    - Code Suggestions is enabled when you have a Duo Pro license assigned to your user account.
    - References:
      - [Code Suggestions on GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/saas.html)
      - [Enable Code Suggestions on self-managed GitLab](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html#enable-code-suggestions-on-self-managed-gitlab)

 #### Alternative Option

 Ask GitLab Duo Chat :robot: and follow the instructions

  ```plaintext
  How to enable Code Suggestions?
  ```

  ```plaintext
  How to enable experimental and beta AI features?
  ```

## :memo: Plan Our Work

### 1\. Create the issue

We know from our project manager that our task is to secure our GitLab pipeline and fix any pressing vulnerabilities that we find. We will use GitLab's Plan capabilities to track our work.

- [ ] To create our first issue:

1. Use the left hand navigation menu to click through **Plan > issues**
2. Next click **New Issue**
3. Give our issue a quick name, then click the **tanuki icon** in the description section.
4. On the dropdown click **Generate issue description**, then in the popup write "Secure our CICD pipeline and fix the newfound vulnerabilities"
5. Next click **submit**, then notice that the description is filled in for us.
6. We can then assign the issue to ourselves, set a due date, and click **create issue**

## :checkered_flag: Running Our Pipeline

### 1\. Generate data for your project

You need to execute the pipeline of the project to populate your project with useful information for the remainder of the workshop (like vulnerability report).

- [ ] To execute a pipeline manually:

1. Use the left hand navigation menu to click through **Build \> Pipeline editor** and replace the current pipeline config with the code below:

    ```
    image: docker:latest

    services:
      - docker:dind

    variables:
      CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
      DOCKER_DRIVER: overlay2
      ROLLOUT_RESOURCE_TYPE: deployment
      DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
      RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
      CI_DEBUG_TRACE: "true"
      SECRET_DETECTION_EXCLUDED_PATHS: "Courses, Golden_Demos"


    stages:
      - build
      - test

    include:
      - template: Jobs/Test.gitlab-ci.yml
      - template: Jobs/Container-Scanning.gitlab-ci.yml
      - template: Code-Quality.gitlab-ci.yml
      - template: Jobs/Dependency-Scanning.gitlab-ci.yml
      - template: Jobs/SAST.gitlab-ci.yml
      - template: Jobs/Secret-Detection.gitlab-ci.yml
      - template: Jobs/SAST-IaC.gitlab-ci.yml

    build:
      stage: build
      variables:
        IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      before_script:
        - apt list --installed
      script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $IMAGE .
        - docker push $IMAGE

    sast:
      variables:
          SEARCH_MAX_DEPTH: 12

    xray:
      stage: build
      image: registry.gitlab.com/gitlab-org/code-creation/repository-x-ray:latest
      allow_failure: true
      rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        OUTPUT_DIR: reports
      script:
        - x-ray-scan -p "$CI_PROJECT_DIR" -o "$OUTPUT_DIR"
      artifacts:
        reports:
          repository_xray: "$OUTPUT_DIR/*/*.json"
    ```

  > Please note you can also merge the **_ai-main-branch_** branch into main as well.

2. Once changed scroll down and click **Commit changes**. Then let this pipeline run as we will come back to it later in the workshop.

> The pipeline is now running and we will come back to it later in the workshop. In the meantime, let's test out other use cases with GitLab Duo Chat

### 2\. Troubleshoot Broken Pipelines

- [ ] To fix our broken pipeline:

1. Notice that the pipeline we kicked off just failed the build job. We are going to use the new Root Cause Analysis feature to fix this issue.  This tool makes troubleshooting a failed pipeline a piece of cake.
2. Use the left hand navigation menu to click through **Build \> Pipelines** and click into the last kicked off pipeline.
3. We expect the _build_ job to fail, and once it does, let's click into the job. At the top of the view we want to click **Root cause analysis,** at which point a pop up will appear on the left hand side of the screen with an in depth analysis on why your job failed. In this case its because of the `apt list --installed` line we added to the build job which our image does not support.
4. To fix this lets use the left hand navigation menu to click through **Build > Pipeline Editor**, then remove the code on lines 33-34 where the before_script code is located.
5. Lastly we can scroll down and click **Commit changes** to kick off our working pipeline.

### 3\. Ask more of GitLab Duo Chat

- [ ] Ask GitLab Duo Chat :robot: to do things like write us starting pipeline configs.

  ```plaintext
  Create a .gitlab-ci.yml configuration file for testing and building a Ruby Sinatra application in a GitLab CI/CD pipeline
  ```
- [ ] Ask GitLab Duo Chat :robot: to write documentation for the code.
  1. On the left sidebar, select `Code > Repository`.
  2. Click through lib/tanukiracing/app.rb
  3. First click the project name in the top left, then click through tanuki-racing/lib/tanukiracing/app.rb
  4. Ask GitLab Duo Chat :robot:

    ```plaintext
    write documentation for app.rb
    ```
- [ ] Finally, Ask GitLab Duo Chat :robot: anything. Please share your best questions/responses by commenting on this issue and mentioning your friendly instructors.

## :beetle: Analyze vulnerabilities

### 1\. Understand the security posture of your application

- [ ] Ensure that the most recent pipeline we kicked off is complete
  1. On the left sidebar, select `Build > Pipelines`.
  2. Spend some time taking a look at all of the information provided to you, exploring the job results and information tabs.
- [ ] See the overview of the vulnerabilities of your project
  1. On the left sidebar, select `Secure > Vulnerability report` to view the full report
  2. First, click into any of the vulnerabilities present. Notice that there are a number of vulnerabilities like token leaks & container issues, all of which GitLab will help you quickly fix through policies and the power of one platform.
- [ ] Look for the **Possible SQL Injection** vulnerability
  1. Filter the report as follows:
    - `Status=All statuses`
    - `Severity=Low`
    - `Tool=SAST`.
  2. Select the vulnerability
- [ ] Get more insight about the vulnerability, how it could be exploited, and how to fix it.
  1. At the bottom of the vulnerability’s page, click the `Explain Vulnerability` button.
  2. A popup will open on the right-hand side and you will get an explanation on what a SQL injection risk is and why our application is vulnerable using [GitLab's Explain This Vulnerability](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability) functionality.
  3. Check out the **_Fixed Code_** section and we can see that if we add `sanitize_sql(id)` around our `id` value we will be protected from most attacks. We will use this knowledge later in the workshop.

> If you are curious what triggered this response try clicking **_Show prompt_** to see the full prompt sent to GitLab duo to generate the suggested fix.

## :art: Implement the fix

### 1\. Resolve With AI
- [ ] Before we jump in and use Code Suggestions to help us fix our security vulnerabilities, we are also going to use the _Resolve with AI_ feature:

1. On the left sidebar, select `Secure > Vulnerability report` to view the full report again

2. Filter the report as follows:
    - `Status=All statuses`
    - `Severity=Critical`
    - `Tool=SAST`.
3. Click into any of the shown vulnerabilities. Once one the vulenerability view go ahead and click **Resolve with AI**.
4. This will then open up a new Merge Request that fixes our vulnerability. Click the **Changes** tab to view the fix that we are bringing in.
5. We can check back in on this later to confirm that our vulenerability is fixed, but in the mean time will move on to the next section.

### 2\. Resolve SQL Injection
- [ ] Now that we have more context around the SQL injection vulnerability lets go try to fix it and do some development with GitLab Code Suggestions:

1. Before we make any code changes we will want to create a merge request to track our work.
2. We already have an issue to track this work, so lets use the left hand navigation to click through **Plan \> Issues**.
3. Click into the issue we created earlier, then click **Create merge request**
4. On the resulting page uncheck **Mark as draft**, leave all other settings as is and scroll to the bottom then click **Create merge request**.
5. Next click **Code** in the top right, then click **Open in Web IDE**.
6. Once in the IDE navigate through **lib/tanukiracing/db.rb** and take a look at the function on line 42. Lets used what we learned using Explain This Vulnerability to change the line of code to this:

```plaintext
Leaderboard.first.where("player = ?", sanitize_sql(id))
```

> Please note that the suggested change my have been different depending on which sql vulnerability you looked at

### 3\. Coding With Duo Chat
- [ ] What if we wanted to double check the code we added was secure? We can use Duo Chat in the IDE to do so:

1. Using the left hand menu options still within the web ide we will want to click the bottom **GitLab Duo Chat** AI Icon.
2. Once here we can see that we are provided the same intro commands as offered in the GitLab UI. We want to start by copying the function we just editted, then sending it with the prompt below:

    ```
    Can you scan this code snippet for vulnerabilities:

    {your code snippet}
    ```
3. Not only are we able to check if our code is secure, we can also generate the unit tests right from here as well. Lets next ask the below prompt with the same code snippet:
    
    ```
    Can you provide a unit test for the code snippet below:

    {your code snippet}
    ```
4. Can also access these features without opening the chat. Go ahead and choose a function in db.rb, highlight it, then click right click.
5. This will open a dropdown menu where we can hover over the _GitLab Duo Chat_ and either chose **Explain Selected Code/Generate Tests/Refactor**


### 4\. Add A New Method

- [ ] Notice that our db.rb file does not have a delete leader method, lets fix that:

1. Our project manager has made it clear we need this asap to get rid of fake records, so lets go ahead and use the prompt below on a new line after the **get_specific** to generate this function:

```plaintext
# write a delete leaderboard function using active record
```

2. We are then going to navigate to the **_lib/tanukiracing/app.rb_** file to use the new method that we wrote. On a new line within the class use the prompt below to generate our new delete route:

```plaintext
# write a post route that calls the delete_leader function from TanukiRacing::DB
```

> Please note that Code Suggestions uses your current file as reference for how to write the function, so you may need to do some slight editing for the final result

### 5\. Create A New Class

- [ ] Your Project Manager has also asked you to write a brand new calculator class that will eventually be used to add up player track times:

1. First right click the **lib/tanukiracing** folder then right-click **new file**. Name this new file **_calc.rb_**.
2. We then want to add the prompt below to let Code Suggestions know what we are trying to write:

```plaintext
 #define a calculator class for other functions to use
```

3. Click enter and then wait a second for the suggestion to come in. As you are given suggestions, hit the TAB key to accept them. If it ever gets stuck try hitting the spacebar or enter.
4. Code suggestions will write a very in depth calculator function and eventually will loop but feel free to stop it after 5 methods.
5. Code Suggestions dosent just work for ruby files either, and it supports multiple languages per project. Navigate into the **ai-sandbox/** folder for a list of currently up to date projects.
6. Choose one of the projects and test out code suggestions to write a hello world example or something more advanced. Your Instructor will give you time to do this now, but also keep in mind that you have access to the infra for another 48 hours to test what you want.
7. Now we want to commit this code to main. Go ahead and click the **Source Control** button on the left hand side and write a commit message. Next click **Commit & Push**.
8. Next on the resulting dropdown make sure you click commit to our mr branch, then on the popup click the **Go to merge request** button.

## :microscope: Review the code

### 6\. AI In The MR

- [ ] Now that we are back in our MR we should see that our code changes have kicked off another run of our pipeline:

1. We have made a number of changes, so lets use the AI **View summary notes** button to add a detailed comment around all of the changes we have made.
2. There are a [few ways to do this](https://docs.gitlab.com/ee/user/project/merge_requests/ai_in_merge_requests.html#summarize-merge-request-changes), but we are going to start by clicking **Edit** in the top right.
3. Then on the resulting page we should see all of our existing Merge Request information. We can also click **Summaize code changes** which will generate a full description of all of the changes in our merge request.
4. Once thats done scroll down to the bottom of the page and click **Save changes**.

## :rocket: Conclusion

1. That is the end of the hands on portion for this lab, but if you check out the [issues](https://gitlab.com/gitlab-learn-labs/sample-projects/ai-workshop/workshop-project/-/issues) there are additional steps to enable code suggestions in VSC and show off some of our Plan stage AI/ML features
